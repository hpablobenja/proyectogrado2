
<?php 
include "sesion.php";
include "inc/header.php";
include "lib/config.php";  
include "lib/database.php";  
?>
<div class="col-sm-12">
   <?php 
    $db=new Database();
    if(isset($_POST['submit'])){
        /*por terminos de seguridad*/
        $nombre  = mysqli_real_escape_string($db->link, $_POST['name']);
    $telefono = mysqli_real_escape_string($db->link, $_POST['telefono']);
        $email = mysqli_real_escape_string($db->link, $_POST['email']);
        $mensaje = mysqli_real_escape_string($db->link, $_POST['mensaje']);
        if($nombre=='' || $email=='' || $mensaje=='' || $telefono==''){
            $error="Los campos no deben estar vacios";
        }else{
            $query="INSERT INTO mensajes(nombre,telefono,correo,mensaje) Values('$nombre','$telefono','$email','$mensaje')";
            $create=$db->insert($query);
        }
    }
  ?>
</div>
<div class="col-sm-12">
    <?php 
        if(isset($error)){
            echo "<div class='alert alert-danger'><span>".$error."</span></div>";
        }
    ?>
  </div>
<div class="table-container">
    <form action="contactos.php" method="POST">
    <div class="contacto1">
     <p>Horarios de atención: fines de semana de 10 a 18 hrs</p>
     <p>Informes y reservas al 78918703 o 71577800 también en nuestra página Mikhuy Bolivia. NO TENEMOS SUCURSALES por el momento</p>
     <p>Estamos ubicados en la ciudad de El Alto, zona San José de Yunguyo entre Avenida Santa Fé y Apurimac (casa colonial)</p>
    </div>
    <div class="contacto2">
     <div class="form-group">
            <label class="text-infor">Nombre: </label>
            <input type="text" name="name" id="name" placeholder="Nombre" class="form-control">
        </div>
     <div class="form-group">
            <label class="text-infor">Celular: </label>
            <input type="text" name="telefono" id="telefono" placeholder="Celular" class="form-control">
        </div>
     <div class="form-group">
            <label class="text-infor">Correo: </label>
            <input type="text" name="email" id="email" placeholder="Correo" class="form-control">
        </div>
     <div class="form-group">
            <label class="text-infor">Mensaje: </label>
            <textarea type="text" name="mensaje" id="mensaje" placeholder="Mensaje" class="form-control" cols="30" rows="10"></textarea>
        </div>
    <div class="form-group">
            <button type="submit" name="submit" value="submit" class="btn btn-primary">Enviar</button>
            <button><a class="btn btn-success" href="contactos.php">Limpiar</a></button>
    </div>  
    </div> 
<br>
</form>
</div>
<br><br>
<?php include 'inc/footer.php'; ?>
<?php 
	include 'inc/header.php';
	include "lib/config.php";
	include "lib/Database.php";
 ?>
 <?php 
 	$db=new Database();
 	if(isset($_POST['submit'])){
 		/*por terminos de seguridad*/
 		$name  = mysqli_real_escape_string($db->link, $_POST['name']);
 		$email = mysqli_real_escape_string($db->link, $_POST['email']);
 		$skill = mysqli_real_escape_string($db->link, $_POST['skill']);
 		if($name=='' || $email=='' || $skill==''){
 			$error="Los campos no deben estar vacios";
 		}else{
 			$query="INSERT INTO tb_user(name,email,skill) Values('$name','$email','$skill')";
 			$create=$db->insert($query);
 		}
 	}
  ?>
  <div class="col-sm-12">
  	<?php 
  		if(isset($error)){
  			echo "<div class='alert alert-danger'><span>".$error."</span></div>";
  		}
  	?>
  </div>
  <div class="col-sm-12">
  	<form action="create.php" method="POST">
  		<div class="form-group">
  			<label class="text-info">Nombre: </label>
  			<input type="text" name="name" id="name" placeholder="Introduzca su nombre" class="form-control">
  		</div>
  		<div class="form-group">
  			<label class="text-info">Email: </label>
  			<input type="text" name="email" id="email" placeholder="Introduzca correo" class="form-control">
  		</div>
  		<div class="form-group">
  			<label class="text-info">Skill: </label>
            <select name="skill" id="skill" class="form-control" required="">
              <option value="PHP">php</option>
              <option value="JAVA">java</option>
              <option value=".NET">.net</option>
              <option value="PYTHON">python</option>
              <option value="POSTGRE">postgre</option>
              <option value="ANDROID">android</option>
              <option value="BASEDATOS">basedatos</option>
            </select>
  		</div>
      <div class="form-group">
        <label class="text-info">Fecha: </label>
        <input type="text" name="fecha" id="email" placeholder="Introduzca fecha" class="form-control">
      </div>
  		 <div class="form-group">
  			<button type="submit" name="submit" value="submit" class="btn btn-primary">Guardar</button>
  			<button type="reset" value="Cancel" class="btn btn-success">Limpiar</button>
  		</div>
  	</form>

	<div class="form-group">   
	<div class="form-group">
    
    <span class="label label-primary" id="vol"><a href="index.php">SALIR</a></span><br>
    <label class="label label-primary">
        <span><a href="index_excel.php" class="btn btn-success">EXPORTAR A EXCEL</a></span>
        <span><a href="index_word.php" class="btn btn-primary">EXPORTAR A WORD</a></span>
        <span><a href="index_pdf.php" class="btn btn-success">EXPORTAR A PDF</a></span>
    </label>
    </div>
  </div>
	<?php include 'inc/footer.php';?>
  </div>
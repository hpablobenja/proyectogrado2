<!DOCTYPE html>
<html lang="en">
<head>

  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="formulario login">
  <meta name="KEYWORDS" content="formulario, logueo, login">

  <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="css/estilos.login.css">
  <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
  <title>Bienvenido Restaurante Mikuy</title>
</head>

<body>

<div class="row">
  <div class="col-md-12">
    <div class="conta">
   <h1>RESTAURANTE&nbsp;&nbsp;&nbsp;<i class="fa fa-cutlery"></i> MIKUY</h1>        
    <form class="" action="checkLogin.php" method="post">
      <?php
            error_reporting(E_ALL ^ E_NOTICE);
            if($_GET["error"]=="si")
               {
                echo '<div class="alert alert-danger" role="alter"><center><strong>Ops!-Verifica tus datos.</strong></center></div>';
                }  
                  else{echo "";}
                ?>
     <hr class="hr-success"/>
     <div>
      <img src="img/loguito.png" id="avatar"> 
     </div>
      <div class="form-group">
      <i class="fa fa-user"></i> <label for="nombreusuario"><b> Nombre del Usuario</b></label>      
      <input type="text" name="user" id="user" class="form-control" placeholder="Nombre Usuario ">
      </div>
    <div class="form-group">
        <i class="fa fa-unlock-alt"></i> <label for="password"><b> Contraseña del  Usuario</b></label> 
      <input type="password" name="pass" id="pass" class="form-control" placeholder="Contraseña Usuario">
    </div><br>
    <div class="form-group">
      <input type="submit" name="submit" value="INGRESAR AL SISTEMA" class="btn btn-primary btn-lg btn-block">
      <span><a class="btn btn-success btn-lg btn-block" href="index.php">Limpiar</a></span>
  </div>
    <span><a class="btn btn-success btn-lg btn-danger" href="nuevousario.php">Registrar un nuevo usuario</a></span>
    </form>
    </div>
  </div>
</div>
</body>
</html>
<?php 
include "sesion.php";
include "inc/header.php";
include "lib/config.php";  
include "lib/database.php";  
?>
 <?php
      $id=$_GET['id'];
        echo $id;
        $db =new database();
        $query="SELECT * FROM pedidos WHERE id_pedido=$id";
        $cambio=$db->select($query);
        while ($row = $cambio->fetch_assoc()) {
          $nom= $row['nombre'];
          $precio=$row['precio_total'];
          $fecha= $row['fecha'];
          $hora= $row['hora'];
        } 
        if(isset($_POST['submit'])){
          $db =new database();
          $nom=mysqli_real_escape_string($db->link, $_POST['nom']);
          $precio=mysqli_real_escape_string($db->link, $_POST['precio']);
          $fecha=mysqli_real_escape_string($db->link, $_POST['fecha']);
          $hora=mysqli_real_escape_string($db->link, $_POST['hora']);
              
          $query="UPDATE  pedidos SET nombre = '$nom', precio_total= '$precio', fecha = '$fecha', hora='$hora' WHERE id_pedido = '$id'";
          
          $res=$db->update($query);
          
          /*if($res>0)
          {           
            echo '<script> self.location="principal.php?msg=ok";  </script>'; }
          else{
          echo '<script>self.location="principal.php?msg=error";  </script>'; 
          } */                
        }     
    ?>      
      <form action="modificarpedido.php?id=<?php echo $id;?>" class="formulario col-md-12"  method="POST">
        <?php
            if(isset($error)){
              echo "<div class='alert-danger'> <span>".$error."</span></div>";
            }
        ?>
        <h4 class="text-center">EDITAR PEDIDO</h4>
        <div class="form-group">
          <label for="" class="col-form-label" >Nombre(s): (*)</label>
          <input type="text" class="form-control text-secondary" value="<?php echo $nom ?>"  placeholder="Introduzca su Nombre"
                  name="nom" id="nom" required="">
        </div>
        <div class="form-group">
          <label for="" class="col-form-label" >Precio Total(s): (*)</label>
          <input type="text" class="form-control text-secondary" value="<?php echo $precio ?>"  placeholder="Introduzca el precio"
                  name="precio" id="precio" required="">
        </div>
        <div class="form-group">
          <label for="" class="col-form-label" >Fecha: (*)</label>
          <input type="text" autofocus class="form-control " value="<?php echo $fecha ?>"  placeholder="Introduzca fecha"
                  name="fecha" id="fecha" required="">
        </div>
              
        
        <div class="form-group">
          <label for="" class="col-form-label">Hora: (*)</label>
          <input type="text" autofocus class="form-control " value="<?php echo $hora ?>"  placeholder="Introduzca la hora"
                  name="hora" id="hora" required="">                 
        </div>
                
        
        <div class="col-md-12 form-group ">
          <button type="submit" class="btn btn-primary   btn-lg" name="submit"
                value="submit">Guardar</button>
          <button type="reset" class="btn btn-danger  btn-lg" value="Cancel">Eliminar</Button>
          <span ><strong><a class="btn btn-primary  btn-lg" href="principal.php"><i class="fa fa-close"></i>Cancelar</a></strong></span>
          
        </div>
        
      </form> 

<?php include 'inc/footer.php';?>
<?php 
include "sesion.php";
include "inc/header.php";
include "lib/config.php";  
include "lib/database.php";  
?>
 <?php
      $id=$_GET['id'];
        echo $id;
        $db =new database();
        $query="SELECT * FROM reservas WHERE id_reservas=$id";
        $cambio=$db->select($query);
        while ($row = $cambio->fetch_assoc()) {
          $nom= $row['nombre'];
          $fecha= $row['fecha'];
          $hora= $row['hora'];
          $mesa=$row['numero_mesa'];
        } 
        if(isset($_POST['submit'])){
          $db =new database();
          $nom=mysqli_real_escape_string($db->link, $_POST['nom']);
          $fecha=mysqli_real_escape_string($db->link, $_POST['fecha']);
          $hora=mysqli_real_escape_string($db->link, $_POST['hora']);
          $mesa=mysqli_real_escape_string($db->link, $_POST['mesa']);
              
          $query="UPDATE  reservas SET nombre = '$nom', fecha = '$fecha', hora='$hora' , numero_mesa= '$mesa' WHERE id_reservas = '$id'";
          
          $res=$db->update($query);
          
          /*if($res>0)
          {           
            echo '<script> self.location="principal.php?msg=ok";  </script>'; }
          else{
          echo '<script>self.location="principal.php?msg=error";  </script>'; 
          } */                
        }     
    ?>      
      <form action="modificarreserva.php?id=<?php echo $id;?>" class="formulario col-md-12"  method="POST">
        <?php
            if(isset($error)){
              echo "<div class='alert-danger'> <span>".$error."</span></div>";
            }
        ?>
        <h4 class="text-center">EDITAR RESERVA</h4>
        <div class="form-group">
          <label for="" class="col-form-label" >Nombre(s): (*)</label>
          <input type="text" class="form-control text-secondary" value="<?php echo $nom ?>"  placeholder="Introduzca su Nombre"
                  name="nom" id="nom" required="">
        </div>
        <div class="form-group">
          <label for="" class="col-form-label" >Fecha: (*)</label>
          <input type="text" autofocus class="form-control " value="<?php echo $fecha ?>"  placeholder="Introduzca fecha"
                  name="fecha" id="fecha" required="">
        </div>
              
        
        <div class="form-group">
          <label for="" class="col-form-label">Hora: (*)</label>
          <input type="text" autofocus class="form-control " value="<?php echo $hora ?>"  placeholder="Introduzca la hora"
                  name="hora" id="hora" required="">                 
        </div>
        <div class="form-group">
          <label for="" class="col-form-label" >Numero mesa(s): (*)</label>
          <input type="text" class="form-control text-secondary" value="<?php echo $mesa ?>"  placeholder="Introduzca numero de mesa"
                  name="mesa" id="mesa" required="">
        </div>  
        
        <div class="col-md-12 form-group ">
          <button type="submit" class="btn btn-primary   btn-lg" name="submit"
                value="submit">Guardar</button>
          <button type="reset" class="btn btn-danger  btn-lg" value="Cancel">Eliminar</Button>
          <span ><strong><a class="btn btn-primary  btn-lg" href="principal.php"><i class="fa fa-close"></i>Cancelar</a></strong></span>
          
        </div>
        
      </form> 

<?php include 'inc/footer.php';?>
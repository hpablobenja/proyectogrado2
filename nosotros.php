
<?php 
include "sesion.php";
include "inc/header.php";
include "lib/config.php";  
include "lib/database.php";  
?>
<div class="col-sm-12">
    <?php
    $db = new database();
    $query = "SELECT* FROM tb_user";
    $read = $db->select($query);
    ?>
    
    <?php
    if(isset($_GET['msg'])){
    echo "<div class='alert alert-success'><span>".$_GET['msg']."</span></div>";
    }
    ?>
</div>

<div class="table-container">
    <table class="table table-hover">
        <tr>
            <th scope="col">Serial</th>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">Skill</th>
            <th scope="col">Action</th>
        </tr>

        <?php if($read){?>
        <?php
        $i=1;
        while($row = $read->fetch_assoc()){
            ?>
            <tr>
                <td><?php echo $i++ ?></td>
                <td><?php echo $row['name']; ?></td>
                <td><?php echo $row['email']; ?></td>
                <td><?php echo $row['skill']; ?></td>
                <td><a href="update.php?id=<?php echo urlencode($row['id']); ?>" class="btn btn-primary btn-sm">EDITAR</a></td>
             </tr>

             <?php } ?>
             <?php} else {?>
            
             <?php } ?>
    </table>
    <div class="form-group">
    <span class="label label-primary"><a href="create.php">CREAR NUEVO USUARIO</a></span>
    <span class="label label-primary" id="vol"><a href="index.php">SALIR</a></span><br>
    <label class="label label-primary">
        <span><a href="index_excel.php" class="btn btn-success">EXPORTAR A EXCEL</a></span>
        <span><a href="index_word.php" class="btn btn-primary">EXPORTAR A WORD</a></span>
        <span><a href="index_pdf.php" class="btn btn-success">EXPORTAR A PDF</a></span>
    </label>
    </div>
</div>
<?php include 'inc/footer.php'; ?>
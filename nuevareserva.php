<?php 
	include 'inc/header.php';
	include "lib/config.php";
	include "lib/Database.php"; 
 ?>
 <?php 
 	$db=new Database();
 	if(isset($_POST['submit'])){
 		/*por terminos de seguridad*/
    $id=$_GET['id'];
 		$nombre  = mysqli_real_escape_string($db->link, $_POST['name']);
    $fecha = mysqli_real_escape_string($db->link, $_POST['fecha']);
 		$hora = mysqli_real_escape_string($db->link, $_POST['hora']);
 		$nro_mesa = mysqli_real_escape_string($db->link, $_POST['nro_mesa']);
 		if($nombre=='' || $fecha=='' || $hora=='' || $nro_mesa==''){
 			$error="Los campos no deben estar vacios";
 		}else{
 			$query="INSERT INTO reservas(nombre,fecha,hora,numero_mesa,id_usuario) Values('$nombre','$fecha','$hora','$nro_mesa','$id')";
 			$create=$db->insert($query);
 		}
 	}
  ?>
  <div class="col-sm-12">
  	<?php 
  		if(isset($error)){
  			echo "<div class='alert alert-danger'><span>".$error."</span></div>";
  		}
  	?>
  </div>
  <div class="col-sm-12">
  	<form action="nuevareserva.php" method="POST">
  		<div class="form-group">
  			<label class="text-info">Nombre: </label>
  			<input type="text" name="name" id="name" placeholder="Introduzca su nombre" class="form-control">
  		</div>
      <div class="form-group">
        <label class="text-info">Hora: </label>
        <input type="text" name="hora" id="hora" placeholder="Introduzca fecha" class="form-control">
      </div>
  		<div class="form-group">
  			<label class="text-info">Fecha: </label>
  			<input type="text" name="fecha" id="fecha" placeholder="Introduzca hora" class="form-control">
  		</div>
  		<div class="form-group">
        <label class="text-info">Numero de mesa: </label>
        <input type="text" name="nro_mesa" id="nro_mesa" placeholder="Introduzca numero de mesa" class="form-control">
      </div>
      
  		 <div class="form-group">
  			<button type="submit" name="submit" value="submit" class="btn btn-primary">Guardar</button>
  			<button><a class="btn btn-success" href="nuevareserva.php">Limpiar</a></button>
  		</div>
  	</form> 
	<?php include 'inc/footer.php';?>
  </div>
<?php 
	include 'inc/header.php';
	include "lib/config.php";
	include "lib/Database.php";
 ?>
 <?php 
 	$db=new Database();
 	if(isset($_POST['submit'])){
 		/*por terminos de seguridad*/
    $id=$_GET['id'];
 		$nombre  = mysqli_real_escape_string($db->link, $_POST['name']);
    $fecha = mysqli_real_escape_string($db->link, $_POST['fecha']);
 		$hora = mysqli_real_escape_string($db->link, $_POST['hora']);
 		$costo = mysqli_real_escape_string($db->link, $_POST['costo']);
 		if($nombre=='' || $fecha=='' || $hora=='' || $costo==''){
 			$error="Los campos no deben estar vacios";
 		}else{
 			$query="INSERT INTO pedidos(nombre,fecha,hora,precio_total,id_usuario) Values('$nombre','$fecha','$hora','$costo','$id')";
 			$create=$db->insert($query);
 		}
 	}
  ?>
  <div class="col-sm-12">
  	<?php 
  		if(isset($error)){
  			echo "<div class='alert alert-danger'><span>".$error."</span></div>";
  		}
  	?>
  </div>
  <div class="col-sm-12">
  	<form action="nuevopedido.php" method="POST">
  		<div class="form-group">
  			<label class="text-info">Nombre: </label>
  			<input type="text" name="name" id="name" placeholder="Introduzca su nombre" class="form-control">
  		</div>
      <div class="form-group">
        <label class="text-info">Fecha: </label>
        <input type="date" name="fecha" id="fecha" placeholder="Introduzca Fecha" class="form-control">
      </div>
  		<div class="form-group">
  			<label class="text-info">Hora: </label>
  			<input type="time" name="hora" id="hora" placeholder="Introduzca Hora" class="form-control">
  		</div>
  		<div class="form-group">
  			<label class="text-info">Costo: </label>
        <input type="text" name="costo" id="costo" placeholder="Introduzca costo" class="form-control">
  		</div>
      
  		 <div class="form-group">
  			<button type="submit" name="submit" value="submit" class="btn btn-primary">Guardar</button>
  			<button type="reset" value="Cancel" class="btn btn-success">Limpiar</button>
  		</div>
  	</form>

	<div class="form-group">   
  </div>
	<?php include 'inc/footer.php';?>
  </div>
<?php 
	include 'inc/header.php';
	include "lib/config.php";
	include "lib/Database.php";
 ?>
 <?php 
 	$db=new Database();
 	if(isset($_POST['submit'])){
 		/*por terminos de seguridad*/
    $id=$_GET['id'];
 		$nombre  = mysqli_real_escape_string($db->link, $_POST['name']);
    $categoria = mysqli_real_escape_string($db->link, $_POST['categoria']);
 		$detalle = mysqli_real_escape_string($db->link, $_POST['detalle']);
 		$costo = mysqli_real_escape_string($db->link, $_POST['costo']);
 		if($nombre=='' || $categoria=='' || $detalle=='' || $costo==''){
 			$error="Los campos no deben estar vacios";
 		}else{
 			$query="INSERT INTO pedidos(Nombre_producto,categoria,Detalle,Costo,id_categoria) Values('$nombre','$categoria','$detalle','$costo','$id')";
 			$create=$db->insert($query);
 		}
 	}
  ?>
  <div class="col-sm-12">
  	<?php 
  		if(isset($error)){
  			echo "<div class='alert alert-danger'><span>".$error."</span></div>";
  		}
  	?>
  </div>
  <div class="col-sm-12">
  	<form action="nuevoproducto.php" method="POST">
  		<div class="form-group">
  			<label class="text-info">Nombre producto: </label>
  			<input type="text" name="name" id="name" placeholder="Introduzca su nombre" class="form-control">
  		</div>
      <div class="form-group">
        <label class="text-info">Categoria producto: </label>
        <input type="text" name="categoria" id="categoria" placeholder="Introduzca categoria" class="form-control">
      </div>
  		<div class="form-group">
  			<label class="text-info">Detalle: </label>
  			<input type="text" name="detalle" id="detalle" placeholder="Introduzca detalle" class="form-control">
  		</div>
  		<div class="form-group">
  			<label class="text-info">Costo: </label>
        <input type="text" name="costo" id="costo" placeholder="Introduzca costo" class="form-control">
  		</div>
      
  		 <div class="form-group">
  			<button type="submit" name="submit" value="submit" class="btn btn-primary">Guardar</button>
  			<button type="reset" value="Cancel" class="btn btn-success">Limpiar</button>
  		</div>
  	</form>

	<div class="form-group">   
  </div>
	<?php include 'inc/footer.php';?>
  </div>
<?php 
	include 'inc/header.php';
	include "lib/config.php";
	include "lib/Database.php";
 ?>
 <?php 
 	$db=new Database();
 	if(isset($_POST['submit'])){
 		/*por terminos de seguridad*/
 		$nombre  = mysqli_real_escape_string($db->link, $_POST['name']);
    $telefono = mysqli_real_escape_string($db->link, $_POST['telefono']);
 		$email = mysqli_real_escape_string($db->link, $_POST['email']);
 		$categoria = mysqli_real_escape_string($db->link, $_POST['categoria']);
 		if($nombre=='' || $email=='' || $categoria=='' || $telefono==''){
 			$error="Los campos no deben estar vacios";
 		}else{
 			$query="INSERT INTO usuarios(nombre,telefono,correo,categoria) Values('$nombre','$telefono','$email','$categoria')";
 			$create=$db->insert($query);
 		}
 	}
  ?>
  <div class="col-sm-12">
  	<?php 
  		if(isset($error)){
  			echo "<div class='alert alert-danger'><span>".$error."</span></div>";
  		}
  	?>
  </div>
  <div class="col-sm-12">
  	<form action="nuevousario.php" method="POST">
  		<div class="form-group">
  			<label class="text-info">Nombre: </label>
  			<input type="text" name="name" id="name" placeholder="Introduzca su nombre" class="form-control">
  		</div>
      <div class="form-group">
        <label class="text-info">Telefono: </label>
        <input type="text" name="telefono" id="telefono" placeholder="Introduzca telefono" class="form-control">
      </div>
  		<div class="form-group">
  			<label class="text-info">Email: </label>
  			<input type="text" name="email" id="email" placeholder="Introduzca correo" class="form-control">
  		</div>
  		<div class="form-group">
  			<label class="text-info">Categoria: </label>
            <select name="categoria" id="categoria" class="form-control" required="">
              <option value="administrador">Administrador</option>
              <option value="empleado">Empleado</option>
              <option value="cliente">Cliente</option>
            </select>
  		</div>
      
  		 <div class="form-group">
  			<button type="submit" name="submit" value="submit" class="btn btn-primary">Guardar</button>
  			<button type="reset" value="Cancel" class="btn btn-success">Limpiar</button>
  		</div>
  	</form>

	<div class="form-group">   
	<div class="form-group">
    
    <span class="label label-primary" id="vol"><a href="index.php">SALIR</a></span><br>
    </div>
  </div>
	<?php include 'inc/footer.php';?>
  </div>

<?php 
include "sesion.php";
include "inc/header.php";
include "lib/config.php";  
include "lib/database.php";  
?>
<?php 
    $dbs = new database();
    $query = "SELECT* FROM usuarios";
    $reads = $dbs->select($query);?>
    <?php $rows = $reads->fetch_assoc()?>
<div class="opciones">
    <button><a type="btn btn-danger" id="agregarpedido" class="btn btn-warning" href="nuevopedido.php?id=<?php echo urlencode($rows['id_usuario']); ?>">Agregar Pedido</a></button>
</div>
<div class="col-sm-12">
    <?php
    $db = new database();
    $query = "SELECT* FROM pedidos";
    $read = $db->select($query);
    ?>
    
    <?php
    if(isset($_GET['msg'])){
    echo "<div class='alert alert-success'><span>".$_GET['msg']."</span></div>";
    }
    ?>
</div>
<div class="table-container">
    <table class="table table-hover" id="table-rwd">
        <tr>
            <th scope="col">Numero</th>
            <th scope="col">Nombre</th>
            <th scope="col">Dia</th>
            <th scope="col">Hora</th>
            <th scope="col">Costo Total</th>
            <th scope="col">Productos</th>
            <th scope="col">Accion</th>
            <th scope="col">Accion2</th>
        </tr>

        <?php if($read){?>
        <?php
        $i=1;
        while($row = $read->fetch_assoc()){
            ?>
            <tr>
                <td><?php echo $i++ ?></td>
                <td><?php echo $row['nombre']; ?></td>
                <td><?php echo $row['fecha']; ?></td>
                <td><?php echo $row['hora']; ?></td>
                <td><?php echo $row['precio_total']; ?></td>
                <td><a href="productos.php?id=<?php echo urlencode($row['id_pedido']); ?>" class="btn btn-primary btn-sm">VER PRODUCTOS</a></td>
                <td><a href="modificarpedido.php?id=<?php echo urlencode($row['id_pedido']); ?>" class="btn btn-primary btn-sm">MODIFICAR PRODUCTOS</a></td>
                <td><a href="eliminarpedido.php?id=<?php echo urlencode($row['id_pedido']); ?>" class="btn btn-primary btn-sm">ELIMINAR</a></td>
             </tr>

             <?php } ?>
             <?php} else {?>
            
             <?php } ?>
    </table>
    <div class="form-group">
    </div>
</div>
<?php include 'inc/footer.php'; ?>
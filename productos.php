
<?php 
include "sesion.php";
include "inc/header.php";
include "lib/config.php";  
include "lib/database.php";  
?>
<div class="col-sm-12">
    <?php
    $db = new database();
    $query = "SELECT* FROM productos";
    $read = $db->select($query);
    ?>
    
    <?php
    if(isset($_GET['msg'])){
    echo "<div class='alert alert-success'><span>".$_GET['msg']."</span></div>";
    }
    ?>
</div>
<div class="opciones">
    <button><a type="btn btn-danger" id="nuevoproducto" class="btn btn-warning" href="nuevoproducto.php">Agregar productos</a></button>
</div>
<div class="table-container">
    <table class="table table-hover">
        <tr>
            <th scope="col">Numero</th>
            <th scope="col">Nombre_producto</th>
            <th scope="col">Categoria_producto</th>
            <th scope="col">Detalle</th>
            <th scope="col">Costo</th>
            <th scope="col">Action</th>
            <th scope="col">Action2</th>
        </tr>

        <?php if($read){?>
        <?php
        $i=1;
        while($row = $read->fetch_assoc()){
            ?>
            <tr>
                <td><?php echo $i++ ?></td>
                <td><?php echo $row['Nombre_producto']; ?></td>
                <td><?php echo $row['categoria_producto']; ?></td>
                <td><?php echo $row['Detalle']; ?></td>
                <td><?php echo $row['Costo']; ?></td>
                <td><a href="modificarproducto.php?id=<?php echo urlencode($row['id_productos']); ?>" class="btn btn-primary btn-sm">EDITAR</a></td>
                <td><a href="eliminarproductos.php?id=<?php echo urlencode($row['id_productos']); ?>" class="btn btn-primary btn-sm">ELIMINAR</a></td>
             </tr>

             <?php } ?>
             <?php} else {?>
            
             <?php } ?>
    </table>
</div>
<?php include 'inc/footer.php'; ?>

<?php 
include "sesion.php";
include "inc/header.php";
include "lib/config.php";  
include "lib/database.php";  
?>
<div class="col-sm-12">
<?php 
    $db = new database();
    $query = "SELECT* FROM usuarios";
    $reads = $db->select($query); ?>
    <?php $rows = $reads->fetch_assoc()?>
    <?php
    if(isset($_GET['msg'])){
    echo "<div class='alert alert-success'><span>".$_GET['msg']."</span></div>";
    }
    ?>
</div>
<div class="opciones">
    <button><a type="btn btn-danger" id="nuevareserva" class="btn btn-warning" href="nuevareserva.php?id=<?php echo urlencode($rows['id_usuario']); ?>">Agregar Reserva</a></button>
</div>
<?php
    $query = "SELECT* FROM reservas";
    $read = $db->select($query);?>
<div class="table-container">
    <table class="table table-hover">
        <tr>
            <th scope="col">Id reserva</th>
            <th scope="col">Nombre</th>
            <th scope="col">Fecha</th>
            <th scope="col">Hora</th>
            <th scope="col">Numero mesa</th>
            <th scope="col">Accion</th>
            <th scope="col">Accion2</th>
        </tr>

        <?php if($read){?>
        <?php
        $i=1;
        while($row = $read->fetch_assoc()){
            ?>
            <tr>
                <td><?php echo $i++ ?></td>
                <td><?php echo $row['nombre']; ?></td>
                <td><?php echo $row['fecha']; ?></td>
                <td><?php echo $row['hora']; ?></td>
                <td><?php echo $row['numero_mesa']; ?></td>
                <td><a href="modificarreserva.php?id=<?php echo urlencode($row['id_reservas']); ?>" class="btn btn-primary btn-sm">EDITAR</a></td>
                 <td><a href="eliminarreserva.php?id=<?php echo urlencode($row['id_reservas']); ?>" class="btn btn-primary btn-sm">ELIMINAR</a></td>
             </tr>
             <?php } ?>
             <?php} else {?>
            
             <?php } ?>
    </table>
</div>
<?php include 'inc/footer.php'; ?>
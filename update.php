<?php 
include "sesion.php";
include "inc/header.php";
include "lib/config.php";  
include "lib/database.php";  
?>
 <?php
      $id=$_GET['id'];
        echo $id;
        $db =new database();
        $query="SELECT * FROM usuarios WHERE id_usuario=$id";
        $cambio=$db->select($query);
        while ($row = $cambio->fetch_assoc()) {
          $nom= $row['nombre'];
          $telefono=$row['telefono'];
          $mail= $row['correo'];
          $categoria= $row['categoria'];
        } 
        if(isset($_POST['submit'])){
          $db =new database();
          $nom=mysqli_real_escape_string($db->link, $_POST['nom']);
          $telefono=mysqli_real_escape_string($db->link, $_POST['telefono']);
          $mail=mysqli_real_escape_string($db->link, $_POST['mail']);
          $categoria=mysqli_real_escape_string($db->link, $_POST['categoria']);
              
          $query="UPDATE  usuarios SET nombre = '$nom', correo= '$mail', telefono = '$telefono', categoria='$categoria' WHERE id_usuario = '$id'";
          
          $res=$db->update($query);
          
          /*if($res>0)
          {           
            echo '<script> self.location="principal.php?msg=ok";  </script>'; }
          else{
          echo '<script>self.location="principal.php?msg=error";  </script>'; 
          } */                
        }     
    ?>      
      <form action="update.php?id=<?php echo $id;?>" class="formulario col-md-12"  method="POST">
        <?php
            if(isset($error)){
              echo "<div class='alert-danger'> <span>".$error."</span></div>";
            }
        ?>
        <h4 class="text-center">EDITAR USUARIO</h4>
        <div class="form-group">
          <label for="" class="col-form-label" >Nombre(s): (*)</label>
          <input type="text" class="form-control text-secondary" value="<?php echo $nom ?>"  placeholder="Introduzca su Nombre"
                  name="nom"
                  id="nom" required="">
        </div>
        <div class="form-group">
          <label for="" class="col-form-label" >Telefono(s): (*)</label>
          <input type="text" class="form-control text-secondary" value="<?php echo $telefono ?>"  placeholder="Introduzca su telefono"
                  name="telefono"
                  id="telefono" required="">
        </div>
        <div class="form-group">
          <label for="" class="col-form-label" >email: (*)</label>
          <input type="mail" autofocus class="form-control " value="<?php echo $mail ?>"  placeholder="Introduzca su email"
                  name="mail"
                  id="mail" required="">
        </div>
              
        
        <div class="form-group">
          <label for="" class="col-form-label">Cargo: (*)</label>
          <input type="text" autofocus class="form-control " value="<?php echo $categoria ?>"  placeholder="Introduzca su categoria"
                  name="categoria"
                  id="categoria" required="">                 
        </div>
                
        
        <div class="col-md-12 form-group ">
          <button type="submit" class="btn btn-primary   btn-lg" name="submit"
                value="submit">Guardar</button>
          <button type="reset" class="btn btn-danger  btn-lg" value="Cancel">Eliminar</Button>
          <span ><strong><a class="btn btn-primary  btn-lg" href="principal.php"><i class="fa fa-close"></i>Cancelar</a></strong></span>
          
        </div>
        
      </form> 

<?php include 'inc/footer.php';?>
<?php 
include "sesion.php";
include "inc/header.php";
include "lib/config.php";  
include "lib/database.php";  
?>
<div class="col-sm-12">
    <?php
    $db = new database();
    $query = "SELECT* FROM usuarios";
    $read = $db->select($query);
    ?>
    
    <?php
    if(isset($_GET['msg'])){
    echo "<div class='alert alert-success'><span>".$_GET['msg']."</span></div>";
    }
    ?>
    <div class="opciones">
    <button><a type="btn btn-danger" id="nuevareserva" class="btn btn-warning" href="nuevousario.php">Registrar Usuario</a></button>
</div>
    
</div>

<div class="col-sm-12">
    <table class="table table-hover">
        <tr>
            <th scope="col">Numero</th>
            <th scope="col">Nombre</th>
            <th scope="col">Telefono</th>
            <th scope="col">Correo</th>
            <th scope="col">Categoria</th>
            <th scope="col">Accion1</th>
            <th scope="col">Accion2</th>
        </tr>

        <?php if($read){?>
        <?php
        $i=1;
        while($row = $read->fetch_assoc()){
            ?>
            <tr>
                <td><?php echo $i++ ?></td>
                <td><?php echo $row['nombre']; ?></td>
                <td><?php echo $row['telefono']; ?></td>
                <td><?php echo $row['correo']; ?></td>
                <td><?php echo $row['categoria']; ?></td>
                <td><a href="update.php?id=<?php echo urlencode($row['id_usuario']); ?>" class="btn btn-primary btn-sm">ACTUALIZAR DATOS</a></td>
                <td><a href="borrar.php?id=<?php echo urlencode($row['id_usuario']); ?>" class="btn btn-primary btn-sm">BORRAR DATOS</a></td>
             </tr>

             <?php } ?>
             <?php} else {?>
            
             <?php } ?>
    </table>
    <div class="form-group">
    </div>
</div>
<?php include 'inc/footer.php'; ?>